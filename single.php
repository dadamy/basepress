<?php get_header(); ?>
<div id='content'>
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<article class="blogpost" id="post-<?php the_ID(); ?>">	
			<h2 class="entry-title"><?php the_title(); ?></h2>
			<div class="entry-content">
				<?php the_content(); ?>
				<?php wp_link_pages(array('before' => 'Pages: ', 'next_or_number' => 'number')); ?>
			</div>
			<?php edit_post_link('Edit this entry','','.'); ?>
		</article>
	<hr />
	<?php comments_template(); ?>
	<?php endwhile; endif; ?>
</div>
<?php get_footer(); ?>