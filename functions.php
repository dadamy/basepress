<?php
// =============================================================================
// Theme Support
// =============================================================================
// Post Thumbnails

if ( function_exists( 'add_theme_support' ) ) { 
    add_theme_support( 'post-thumbnails' );
    // Large, Medium, Small, Custom
    add_image_size('large', 700, '', true);
    add_image_size('medium', 250, '', true);
    add_image_size('small', 120, '', true);
    add_image_size('custom-size', 700, 200, true);
    // Add Menu Support
    add_theme_support( 'nav-menus' );
    
    // Add Custom Background Support
    /*
    add_theme_support('custom-background', array(
    'default-color' => 'FFF',
    'default-image' => get_template_directory_uri() . '/img/bg.jpg'
    ));
    */

    // Add Custom Header Support
    /*
    add_theme_support('custom-header', array(
	    'default-image'			=> get_template_directory_uri() . '/images/headers/default.jpg',
	    'header-text'			=> false,
	    'default-text-color'		=> '000',
	    'width'				=> 1000,
	    'height'			=> 198,
	    'random-default'		=> false,
	    'wp-head-callback'		=> $wphead_cb,
	    'admin-head-callback'		=> $adminhead_cb,
	    'admin-preview-callback'	=> $adminpreview_cb
    ));
    */
    // Switches default core markup for comment form, and comments to output valid HTML5.
    add_theme_support( 'html5', array('comment-form', 'comment-list' ) );
}
// =============================================================================
// Nav Menu & Bar
// =============================================================================
function nav_menu() {
    wp_nav_menu(  
    array( 
        'theme_location' => 'MainNav',
        'container_class' => 'hidden-xs',
        'menu_class' => 'nav nav-pills hide',
        'menu_id' => 'mainmenu'
    ));        
}
// =============================================================================
// Load jQuery
// =============================================================================
function jquery_loader() {
    if(!is_admin()){
        wp_deregister_script('jquery');
        wp_register_script('jquery', "http" . ($_SERVER['SERVER_PORT'] == 443 ? "s" : "") . "://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js", false, null);
        wp_enqueue_script('jquery');
    }
}
// =============================================================================
// Load CSS
// =============================================================================
function css_loader() {
    wp_enqueue_style('bootstrap', get_template_directory_uri().'/css/bootstrap.min.css', false ,'3.3.2', 'all' );
    wp_enqueue_style('fontawesome', get_template_directory_uri().'/css/font-awesome.min.css', false, '4.3', 'all' );
    wp_enqueue_style('style', get_template_directory_uri().'/style.css', false ,'1.0', 'all' );
}
// =============================================================================
// Load JavaScript
// =============================================================================
function js_loader() {
	wp_enqueue_script('bootstrapjs', get_template_directory_uri().'/js/bootstrap.min.js', array('jquery'),'1.0', true );     
}
// =============================================================================
// Sidebars
// =============================================================================
/*
if ( function_exists('register_sidebar') ) {
register_sidebar(array(
		'name' => 'header-widget',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h2 class="widgettitle">',
        'after_title' => '</h2>'
	));
}
*/
// =============================================================================
// Menu
// =============================================================================
function register_navs() {
  register_nav_menus(
    array(
      'MainNav' => 'Main Nav',
      'MobileNav' => 'Mobile Nav'
    )
  );
}
// =============================================================================
// Remove Injected classes and ID's from Navigation <li> items
// =============================================================================
function remove_injected_menu_filter($var)
{
    return is_array($var) ? array() : '';
}
// =============================================================================
// Custom Login Logo
// =============================================================================
function custom_login_logo() {
    echo '<style type="text/css">
        h1 a { background-image:url('.get_bloginfo('template_directory').'/images/login_logo.png) !important; }
    </style>';
}
// =============================================================================
// Custom except length
// =============================================================================
function custom_excerpt_length( $length ) {
    return 40;
}
// =============================================================================
// Custom Except More Text - get rid of []'s
// =============================================================================
function new_excerpt_more($more) {
    return '...';
}
// =============================================================================
// Custom Read More Link
// =============================================================================
function excerpt_read_more_link($output) {
 global $post;
 return $output . '<a href="'. get_permalink($post->ID) . '"> [Read More]</a>';
}
// =============================================================================
// Threaded Comments
// =============================================================================
function enable_threaded_comments()
{
    if (!is_admin()) {
        if (is_singular() AND comments_open() AND (get_option('thread_comments') == 1)) {
            wp_enqueue_script('comment-reply');
        }
    }
}
// =============================================================================
// Add Slug to body class
// =============================================================================
function add_slug_to_body_class($classes)
{
    global $post;
    if (is_home()) {
        $key = array_search('blog', $classes);
        if ($key > -1) {
            unset($classes[$key]);
        }
    } elseif (is_page()) {
        $classes[] = sanitize_html_class($post->post_name);
    } elseif (is_singular()) {
        $classes[] = sanitize_html_class($post->post_name);
    }

    return $classes;
}
// =============================================================================
// Maintenance Mode
// =============================================================================
function wpr_maintenance_mode() {
    if ( !current_user_can( 'edit_themes' ) || !is_user_logged_in() ) {
        wp_die('This site is currently undergoing maintenance. Please come back soon.');
    }
}
// =============================================================================
// Get the page slug, unless it's a blog post
// =============================================================================
function get_slug() {
    global $post;
    if (is_page()) {
        if ($post->post_parent) {
            $post_data = get_post($post->post_parent);
            $slug = $post_data->post_name;
        } else {
            $slug = get_post( $post )->post_name;
        }
    } else {
        $slug = "blog";
    }
    return $slug;
}
// =============================================================================
// Grab First Image in Post
// =============================================================================
function catch_that_image() {
  global $post, $posts;
  $first_img = '';
  ob_start();
  ob_end_clean();
  $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
  $first_img = $matches [1] [0];

  if(empty($first_img)){ //Defines a default image
    $first_img = bloginfo('template_url')."/images/login_logo.png";
  }
  return $first_img;
}
// =============================================================================
// Create the_excerpt, preserving html tags
// =============================================================================
function custom_wp_trim_excerpt($text) {
$raw_excerpt = $text;
if ( '' == $text ) {
    //Retrieve the post content. 
    $text = get_the_content('');
    //Delete all shortcode tags from the content. 
    $text = strip_shortcodes( $text );
    $text = apply_filters('the_content', $text);
    $text = str_replace(']]>', ']]&gt;', $text);
    
    $allowed_tags = '<p>,<a>,<em>,<strong>';
    $text = strip_tags($text, $allowed_tags);
    // Excerpt Length
    $excerpt_word_count = 175; 
    $excerpt_length = apply_filters('excerpt_length', $excerpt_word_count);
    // Excerpt more
    $excerpt_end = ' <a href="'. get_permalink($post->ID) . '">' . '&raquo; [Read More]' . '</a>'; 
    $excerpt_more = apply_filters('excerpt_more', ' ' . $excerpt_end);
    $words = preg_split("/[\n\r\t ]+/", $text, $excerpt_length + 1, PREG_SPLIT_NO_EMPTY);
    if ( count($words) > $excerpt_length ) {
        array_pop($words);
        $text = implode(' ', $words);
        $text = $text . $excerpt_more;
    } else {
        $text = implode(' ', $words);
    }
}
return apply_filters('wp_trim_excerpt', $text, $raw_excerpt);
}
remove_filter('get_the_excerpt', 'wp_trim_excerpt');
add_filter('get_the_excerpt', 'custom_wp_trim_excerpt');
// =============================================================================
// Actions
// =============================================================================
remove_action('wp_head', 'wp_generator'); //Remove Generator Tag
remove_action('wp_head', 'feed_links_extra', 3); // Remove extra feed links
add_action('init', 'jquery_loader'); // load jQuery
add_action('wp_enqueue_scripts', 'css_loader'); // Load CSS
add_action('wp_enqueue_scripts', 'js_loader'); // Load JS
add_action('init', 'register_navs'); // Register nav menus
add_action('login_head', 'custom_login_logo'); // Custom Login Image
add_filter('excerpt_length', 'custom_excerpt_length');
add_filter('excerpt_more', 'new_excerpt_more');
add_filter('the_excerpt', 'excerpt_read_more_link');
// add_action('get_header', 'wpr_maintenance_mode'); // Toggle Maintentance Mode
add_filter('nav_menu_css_class', 'remove_injected_menu_filter', 100, 1); // Remove Navigation <li> injected classes
add_filter('nav_menu_item_id', 'remove_injected_menu_filter', 100, 1); // Remove Navigation <li> injected IDs
add_action('get_header', 'enable_threaded_comments'); // Enable Threaded Comments
add_filter('body_class', 'add_slug_to_body_class'); // Add slug to body class

// remove_action('wp_head', 'rsd_link'); // Display the link to the Really Simple Discovery service endpoint, EditURI link
// remove_action('wp_head', 'wlwmanifest_link'); // Display the link to the Windows Live Writer manifest file.

?>