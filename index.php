<?php get_header();?>
<div id="content" class="row">
<article>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	<?php the_content(); ?>
<?php endwhile; endif; ?>
</article>
</div>
<?php get_footer(); ?>
