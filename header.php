<?php
/**
 * Header template
 *
 * @package WordPress
 * @subpackage Basepress
 * @since Basepress 1.0
 */
?>
<!doctype html>
<html><?php language_attributes(); ?> class="no-js">
<head>

  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <?php if ( file_exists(TEMPLATEPATH .'/favicon.ico') ) : ?>
  <link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/favicon.ico">
  <?php endif; ?>
  <link rel="profile" href="http://gmpg.org/xfn/11" />
  <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
  <link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> Feed" href="<?php echo home_url(); ?>/feed/">
  <title><?php wp_title('|', true, 'right'); bloginfo('name'); ?></title>
  <?php wp_head(); ?>
  
  <!--
  <script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-XXXXXXXX-X']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
-->
</head>
<body <?php body_class(); ?>>
    <!-- Start Bootstrap Mobile Navbar -->
    <div class="navbar navbar-default visible-xs">
      <div class="navbar-header">
        <button type="button" data-toggle="collapse" data-target=".navbar-mobile-collapse" class="navbar-toggle">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
      </div>
      <div class="nav-collapse collapse navbar-mobile-collapse">
            <?php wp_nav_menu(  array( 'theme_location' => 'MobileNav','menu_id' => 'nav' ));  ?>
     </div>
    </div>
    <!-- End Bootstrap Mobile Navbar -->
<div class="container">
	<header role="banner">
		<div id="headerlogo" class="row">
        <img class="col-md-12 img-responsive" src="http://lorempixel.com/1024/400" alt=""/>
    </div>
	</header>
	<nav class='row'>
		<?php nav_menu(); ?>
  </nav>

