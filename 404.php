<?php get_header(); ?>

<div id="notfound">
	<h2>Page Not Found</h2>
	<p>Sorry, I can't find that page.</p>
</div><!--#notfound-->

<?php get_footer(); ?>