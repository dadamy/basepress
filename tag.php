<?php get_header();?>
<div id="content" class="row">
<article>
	<h1 class="read-more">Tag Archive: <?php single_tag_title( '', true ); ?></h1>
    <?php
    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
    query_posts( array( 'post_type' => 'post', 'paged'=>$paged, 'showposts'=>0) );
    if (have_posts()) : while ( have_posts() ) : the_post(); 
	?>
	<div id="post-<?php the_ID(); ?>" class="row">
		<div class="postinfo col-md-12">
			<h2><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title(); ?>"><?php the_title(); ?></a></h2>
			<h4><i class="icon-edit"></i> Posted on <?php the_time('F j, Y'); ?> by <?php the_author(); ?></h4>
		</div>
		<div class="col-md-12">
			<div class="postcontent col-md-8">
				<?php custom_wp_trim_excerpt(the_excerpt()); ?>
			</div>
			<div class="col-md-4">
				<?php the_post_thumbnail( 'medium' ); ?> 
			</div>
			<div class="postbot col-md-12">				
				<h5><?php comments_popup_link( 'No Comments', '1 comment', '% comments', 'comments-link', 'Comments are off for this post'); ?></h5>
			</div>
		</div>
		<hr />
	</div>
	<?php endwhile; endif; ?>
	<ul class="pager navigation">
		<li class="previous"><?php next_posts_link('&#171; Previous Entries') ?></li>
		<li class="next"><?php previous_posts_link('Next Entries &#187; ') ?></li>
	</ul>
</article>
</div><!-- #content -->
<?php get_footer(); ?>

