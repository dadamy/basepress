		</div><!--#container-->
		<footer class="container">
		<hr />
			<div class="footer-social span12">
				<a href="http://www.facebook.com" target="_blank"><i class="fa fa-facebook-official fa-2x"></i></a>
				<a href="http://www.youtube.com" target="_blank"><i class="fa fa-pinterest fa-2x"></i></a>
				<a href="https://www.twitter.com" target="_blank"><i class="fa fa-twitter fa-2x"></i></a>
			</div>
			<div class="footer-copyright span12">
				<a href="http://www.variocreative.com">&copy;<?php echo date("Y"); echo " ";?>Vario Creative. Graphic and Web Design in Central Massachusetts</a>
			</div>
		</footer>
    <!-- Start Bootstrap Dropdown -->
    <script type="text/javascript">
     // Adding the class "dropdown" to li elements with submenus //
     $(document).ready(function(){
          $("ul.sub-menu").parent().addClass("dropdown");
          $("ul.sub-menu").addClass("dropdown-menu");
          $("ul#mainmenu li.dropdown a").addClass("dropdown-toggle");
		  $("ul#nav li.dropdown a").addClass("dropdown-toggle");
          $("ul.sub-menu li a").removeClass("dropdown-toggle");
          $('a.dropdown-toggle').append('<b class="caret"></b>');
		  // Once it's loaded, unhide it.
		 $("ul#mainmenu").removeClass("hide");
		 $("ul#mobilemenu").removeClass("hide");
     });
</script>
	<script type="text/javascript">
		 $(document).ready(function(){
			  $('a.dropdown-toggle')
			  .attr('data-toggle', 'dropdown');
			  });
	</script>
      <!-- End Bootstrap Dropdown -->
   <?php wp_footer(); ?>
</body>

</html>
